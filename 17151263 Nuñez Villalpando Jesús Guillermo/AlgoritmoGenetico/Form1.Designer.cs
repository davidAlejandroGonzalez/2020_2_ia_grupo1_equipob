﻿namespace AlgoritmoGenetico
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.TEXTORICO = new System.Windows.Forms.RichTextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.REGISINDI = new System.Windows.Forms.Button();
            this.INDIVIDUO = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.CONJUNTOFIT = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.MUTACION = new System.Windows.Forms.TextBox();
            this.PRESION = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.GENERACIONES = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.POBLACION = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.LONGCFENOTIPO = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CPFENOTIPO = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CPFEN = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // TEXTORICO
            // 
            this.TEXTORICO.Location = new System.Drawing.Point(449, 3);
            this.TEXTORICO.Name = "TEXTORICO";
            this.TEXTORICO.Size = new System.Drawing.Size(613, 491);
            this.TEXTORICO.TabIndex = 62;
            this.TEXTORICO.Text = "";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(32, 297);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(189, 35);
            this.button3.TabIndex = 61;
            this.button3.Text = "GENERAR INDIVIDUO RANDOM";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(103, 360);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(209, 56);
            this.button2.TabIndex = 60;
            this.button2.Text = "CARGAR GENERACIONES";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // REGISINDI
            // 
            this.REGISINDI.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.REGISINDI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.REGISINDI.Location = new System.Drawing.Point(227, 298);
            this.REGISINDI.Name = "REGISINDI";
            this.REGISINDI.Size = new System.Drawing.Size(189, 35);
            this.REGISINDI.TabIndex = 59;
            this.REGISINDI.Text = "REGISTRAR A INDIVIDUO 1";
            this.REGISINDI.UseVisualStyleBackColor = false;
            this.REGISINDI.Visible = false;
            this.REGISINDI.Click += new System.EventHandler(this.REGISINDI_Click_1);
            // 
            // INDIVIDUO
            // 
            this.INDIVIDUO.Location = new System.Drawing.Point(103, 267);
            this.INDIVIDUO.Name = "INDIVIDUO";
            this.INDIVIDUO.Size = new System.Drawing.Size(338, 20);
            this.INDIVIDUO.TabIndex = 58;
            this.INDIVIDUO.Visible = false;
            this.INDIVIDUO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.INDIVIDUO_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(-122, 271);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 57;
            this.label8.Text = "INDIVIDUO :";
            this.label8.Visible = false;
            // 
            // CONJUNTOFIT
            // 
            this.CONJUNTOFIT.Location = new System.Drawing.Point(234, 231);
            this.CONJUNTOFIT.Name = "CONJUNTOFIT";
            this.CONJUNTOFIT.Size = new System.Drawing.Size(209, 20);
            this.CONJUNTOFIT.TabIndex = 56;
            this.CONJUNTOFIT.Visible = false;
            this.CONJUNTOFIT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CONJUNTOFIT_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1, 234);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(236, 13);
            this.label7.TabIndex = 55;
            this.label7.Text = "CONJUNTO PARA EVALUAR FITNESS :";
            this.label7.Visible = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(120, 156);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 40);
            this.button1.TabIndex = 54;
            this.button1.Text = "SIGUIENTE";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click_1);
            // 
            // MUTACION
            // 
            this.MUTACION.Location = new System.Drawing.Point(202, 131);
            this.MUTACION.MaxLength = 3;
            this.MUTACION.Name = "MUTACION";
            this.MUTACION.Size = new System.Drawing.Size(238, 20);
            this.MUTACION.TabIndex = 53;
            this.MUTACION.Text = "5";
            // 
            // PRESION
            // 
            this.PRESION.Location = new System.Drawing.Point(185, 107);
            this.PRESION.MaxLength = 3;
            this.PRESION.Name = "PRESION";
            this.PRESION.Size = new System.Drawing.Size(255, 20);
            this.PRESION.TabIndex = 52;
            this.PRESION.Text = "30";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(196, 13);
            this.label6.TabIndex = 51;
            this.label6.Text = "PROBABILIDAD DE MUTACION :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(176, 13);
            this.label5.TabIndex = 50;
            this.label5.Text = "PORCENTAJE DE PRESION :";
            // 
            // GENERACIONES
            // 
            this.GENERACIONES.Location = new System.Drawing.Point(216, 84);
            this.GENERACIONES.MaxLength = 1;
            this.GENERACIONES.Name = "GENERACIONES";
            this.GENERACIONES.Size = new System.Drawing.Size(224, 20);
            this.GENERACIONES.TabIndex = 49;
            this.GENERACIONES.Text = "3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(220, 13);
            this.label4.TabIndex = 48;
            this.label4.Text = "CANTIDADES DE GENERACIONES : ";
            // 
            // POBLACION
            // 
            this.POBLACION.Location = new System.Drawing.Point(173, 58);
            this.POBLACION.MaxLength = 2;
            this.POBLACION.Name = "POBLACION";
            this.POBLACION.Size = new System.Drawing.Size(267, 20);
            this.POBLACION.TabIndex = 47;
            this.POBLACION.Text = "10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 13);
            this.label3.TabIndex = 46;
            this.label3.Text = "TAMAÑO DE POBLACION :";
            // 
            // LONGCFENOTIPO
            // 
            this.LONGCFENOTIPO.Location = new System.Drawing.Point(273, 36);
            this.LONGCFENOTIPO.MaxLength = 2;
            this.LONGCFENOTIPO.Name = "LONGCFENOTIPO";
            this.LONGCFENOTIPO.Size = new System.Drawing.Size(167, 20);
            this.LONGCFENOTIPO.TabIndex = 45;
            this.LONGCFENOTIPO.Text = "8";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(275, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "CANTIDAD DE FENOTIPOS POR GENOTIPO : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-123, -17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(281, 13);
            this.label1.TabIndex = 43;
            this.label1.Text = "CANTIDAD DE CARACTERES POR FENOTIPO :";
            // 
            // CPFENOTIPO
            // 
            this.CPFENOTIPO.Location = new System.Drawing.Point(152, -20);
            this.CPFENOTIPO.MaxLength = 1;
            this.CPFENOTIPO.Name = "CPFENOTIPO";
            this.CPFENOTIPO.Size = new System.Drawing.Size(164, 20);
            this.CPFENOTIPO.TabIndex = 42;
            this.CPFENOTIPO.Text = "1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(281, 13);
            this.label9.TabIndex = 63;
            this.label9.Text = "CANTIDAD DE CARACTERES POR FENOTIPO :";
            // 
            // CPFEN
            // 
            this.CPFEN.Location = new System.Drawing.Point(279, 9);
            this.CPFEN.MaxLength = 1;
            this.CPFEN.Name = "CPFEN";
            this.CPFEN.Size = new System.Drawing.Size(164, 20);
            this.CPFEN.TabIndex = 64;
            this.CPFEN.Text = "1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(1059, 496);
            this.Controls.Add(this.CPFEN);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TEXTORICO);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.REGISINDI);
            this.Controls.Add(this.INDIVIDUO);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.CONJUNTOFIT);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.MUTACION);
            this.Controls.Add(this.PRESION);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.GENERACIONES);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.POBLACION);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LONGCFENOTIPO);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CPFENOTIPO);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox TEXTORICO;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button REGISINDI;
        private System.Windows.Forms.TextBox INDIVIDUO;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox CONJUNTOFIT;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox MUTACION;
        private System.Windows.Forms.TextBox PRESION;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox GENERACIONES;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox POBLACION;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox LONGCFENOTIPO;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CPFENOTIPO;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox CPFEN;
    }
}

