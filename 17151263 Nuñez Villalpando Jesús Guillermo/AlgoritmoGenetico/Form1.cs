﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlgoritmoGenetico
{
    public partial class Form1 : Form
    {
        string[,,] individuos;
        string[] evaluador;
        int[,,] matorder;
        int longit = 0;
        int conta = 0, contafeno = 0;
        double sobrevivientes = 0, mutados=0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        
        private void REST_Click(object sender, EventArgs e)
        {
            restablecer();
        }

        public void restablecer()
        {
            CONJUNTOFIT.Visible = false;
            CONJUNTOFIT.Text = "";
            label7.Visible = false;
            INDIVIDUO.Visible = false;
            INDIVIDUO.Text = "";
            label8.Visible = false;
            REGISINDI.Visible = false;
            //primera parte rest
            CPFENOTIPO.Enabled = true;
            CPFENOTIPO.Text = "";
            LONGCFENOTIPO.Enabled = true;
            LONGCFENOTIPO.Text = "";
            POBLACION.Enabled = true;
            POBLACION.Text = "";
            GENERACIONES.Enabled = true;
            GENERACIONES.Text = "";
            PRESION.Enabled = true;
            PRESION.Text = "";
            MUTACION.Enabled = true;
            MUTACION.Text = "";
            button1.Enabled = true;
            button2.Visible = false;
            button3.Visible = false;
            TEXTORICO.Text = "";
            REGISINDI.Enabled = true;
            INDIVIDUO.Enabled = true;
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
                e.Handled = false;
            else if (Char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
                e.Handled = false;
            else if (Char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
                e.Handled = false;
            else if (Char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
                e.Handled = false;
            else if (Char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (Char.IsPunctuation(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
                e.Handled = false;
            else if (Char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (Char.IsPunctuation(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void CONJUNTOFIT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
                e.Handled = false;
            else if (Char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

   

 
   
        public void sacafit()
        {
            try
            {
                sobrevivientes = Convert.ToInt32(POBLACION.Text) * Convert.ToInt32(PRESION.Text);
                sobrevivientes = sobrevivientes / 100;
                sobrevivientes = Math.Floor(sobrevivientes);
                sobrevivientes = Convert.ToInt32(POBLACION.Text) - sobrevivientes;
                mutados = Convert.ToInt32(POBLACION.Text) * Convert.ToInt32(MUTACION.Text);
                mutados = mutados / 100;
                mutados = Math.Floor(mutados);
                TEXTORICO.Text = "";
                TEXTORICO.Text = "CONJUNTO EVALUADOR : ";
                for (int i = 0; i < Convert.ToInt32(LONGCFENOTIPO.Text); i++)
                {
                    TEXTORICO.Text = TEXTORICO.Text + evaluador[i];
                    if (i + 1 < Convert.ToInt32(LONGCFENOTIPO.Text))
                        TEXTORICO.Text = TEXTORICO.Text + "-";
                }
                TEXTORICO.Text = TEXTORICO.Text + "\n";
                for (int i = 0; i < Convert.ToInt32(GENERACIONES.Text); i++)
                {
                    TEXTORICO.Text = TEXTORICO.Text + "\n\nGENEREACION :" + (i + 1) + "\n";
                    //empieza lo bueno
                    for (int i2 = 0; i2 < Convert.ToInt32(POBLACION.Text); i2++)
                    {
                        TEXTORICO.Text = TEXTORICO.Text + "\nINDIVIDUO " + (i2 + 1) + " : ";
                        int contafit = 0;
                        for (int i3 = 0; i3 < Convert.ToInt32(LONGCFENOTIPO.Text); i3++)
                        {
                            TEXTORICO.Text = TEXTORICO.Text + individuos[i, i2, i3];
                            if (i3 + 1 < Convert.ToInt32(LONGCFENOTIPO.Text))
                                TEXTORICO.Text = TEXTORICO.Text + "-";
                            if (individuos[i, i2, i3] == evaluador[i3])
                                contafit++;
                        }
                        TEXTORICO.Text = TEXTORICO.Text + "        FITNESS: " + contafit;
                        matorder[i, i2, 0] = i2;
                        matorder[i, i2, 1] = contafit;
                    }
                    TEXTORICO.Text = TEXTORICO.Text + "\n\n" + "DEBIDO A LA PRESIÓN SOLO SOBREVIVEN :" + sobrevivientes + " INDIVIDUOS CON EL FITNESS MAS ALTO Y DEBIDO AL PORCENTAJE DE MUTACION " + mutados + " INDIVIDUOS DE LA SIGUIENTE GENERACION MUTARAN";
                    //ordenar matriz
                    ordena(i);
                    //calcular nueva generacion
                    if (i < Convert.ToInt32(GENERACIONES.Text) - 1)
                    {
                        var random = new Random();
                        int indi1 = 0, indi2 = 0, bandera1 = 0, bandera2 = 0;
                        for (int i2 = 0; i2 < Convert.ToInt32(POBLACION.Text); i2++)
                        {
                            bandera1 = 0;
                            bandera2 = 0;
                            indi1 = 1; indi2 = 1;
                            while (((bandera1 == 0) && (bandera2 == 0)) || (indi1 == indi2))
                            {
                                if (indi1 == indi2)
                                { bandera1 = 0; bandera2 = 0; }
                                if (bandera1 == 0)
                                    indi1 = matorder[i, random.Next((Convert.ToInt32(LONGCFENOTIPO.Text) / 4), Convert.ToInt32(sobrevivientes) - (Convert.ToInt32(LONGCFENOTIPO.Text) / 4)), 0];
                                if (bandera2 == 0)
                                    indi2 = matorder[i, random.Next((Convert.ToInt32(LONGCFENOTIPO.Text) / 4), Convert.ToInt32(sobrevivientes) - (Convert.ToInt32(LONGCFENOTIPO.Text) / 4)), 0];
                                for (int x = 0; x < sobrevivientes; x++)
                                {
                                    if (indi1 == matorder[i, x, 0])
                                        bandera1 = 1;
                                    if (indi2 == matorder[i, x, 0])
                                        bandera2 = 1;
                                }
                            }
                            //TEXTORICO.Text = TEXTORICO.Text + "\nINDIVIDUO 1 ELEJIDOS : " + (indi1 + 1);
                            //TEXTORICO.Text = TEXTORICO.Text + "\nINDIVIDUOS 2 ELEJIDOS : " + (indi2 + 1);
                            //tomamos parte de cada individuo 
                            int repo = random.Next(0, Convert.ToInt32(LONGCFENOTIPO.Text));
                            int b1 = 0;
                            for (b1 = 0; b1 < repo; b1++)
                            {
                                individuos[i + 1, i2, b1] = individuos[i, indi1, b1];
                            }
                            for (int b2 = b1; b2 < Convert.ToInt32(LONGCFENOTIPO.Text); b2++)
                            {
                                individuos[i + 1, i2, b2] = individuos[i, indi2, b2];
                            }

                        }
                        //mutacion
                        string num = "";
                        for (int c1 = 0; c1 < mutados; c1++)
                        {
                            num = "";
                            for (int ixx = 0; ixx < Convert.ToInt32(CPFENOTIPO.Text); ixx++)
                            {
                                var value = random.Next(1, 9);
                                num = num + Convert.ToInt32(value);

                            }
                            //MessageBox.Show(num);
                            individuos[i + 1, c1, random.Next(0, Convert.ToInt32(LONGCFENOTIPO.Text))] = num;
                            //MessageBox.Show("" + c1);
                        }
                    }
                    //termina lo bueno
                }
            }
            catch (Exception)
            {
                MessageBox.Show("A OCURRIDO UN ERROR 2");
            }
        }

        public void ordena(int gen)
        {
            int a = 0, aux = Convert.ToInt32(POBLACION.Text);
            do
            {
                a = 0;
                for (int i = 0; i < Convert.ToInt32(POBLACION.Text); i++)
                {
                    if (matorder[gen, i, 1] < matorder[gen, i + 1, 1])
                    {
                        matorder[gen, aux, 0] = matorder[gen, i, 0]; matorder[gen, aux, 1] = matorder[gen, i, 1];
                        matorder[gen, i, 0] = matorder[gen, i + 1, 0]; matorder[gen, i, 1] = matorder[gen, i + 1, 1];
                        matorder[gen, i + 1, 0] = matorder[gen, aux, 0]; matorder[gen, i + 1, 1] = matorder[gen, aux, 1];
                        a++;
                    }
                }
            } while (a != 0);
            muestramatorder(gen);
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToDouble(PRESION.Text) <= 100 && Convert.ToDouble(PRESION.Text) >= 0)
                {
                    if (Convert.ToDouble(MUTACION.Text) <= 100 && Convert.ToDouble(MUTACION.Text) >= 0)
                    {
                        longit = (Convert.ToInt32(CPFENOTIPO.Text) * Convert.ToInt32(LONGCFENOTIPO.Text));
                        CONJUNTOFIT.MaxLength = longit;
                        CONJUNTOFIT.Visible = true;
                        label7.Visible = true;
                        INDIVIDUO.Visible = true;
                        INDIVIDUO.MaxLength = CONJUNTOFIT.MaxLength;
                        label8.Visible = true;
                        REGISINDI.Visible = true;
                        button3.Visible = true;
                        CPFENOTIPO.Enabled = false;
                        LONGCFENOTIPO.Enabled = false;
                        POBLACION.Enabled = false;
                        GENERACIONES.Enabled = false;
                        PRESION.Enabled = false;
                        MUTACION.Enabled = false;
                        button1.Enabled = false;
                        individuos = new string[Convert.ToInt32(GENERACIONES.Text), Convert.ToInt32(POBLACION.Text), Convert.ToInt32(LONGCFENOTIPO.Text)];
                        matorder = new int[Convert.ToInt32(GENERACIONES.Text), Convert.ToInt32(POBLACION.Text) + 1, 2];
                    }
                    else
                    {
                        MessageBox.Show("LA PROBABILIDAD DE MUTACION TIENE QUE ESTAR ENTRE 100% Y 0%");

                    }
                }
                else
                {
                    MessageBox.Show("LA PRESION TIENE QUE ESTAR ENTRE 100% Y 0%");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("DEBES LLENAR LOS DATOS CORRECTAMENTE");
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            var random = new Random();
            string num = "";
            for (int i = 0; i < longit; i++)
            {
                var value = random.Next(1, 9);
                num = num + Convert.ToInt32(value);

            }
            INDIVIDUO.Text = "" + num;
            REGISINDI.PerformClick();
        }

        private void REGISINDI_Click_1(object sender, EventArgs e)
        {
            string a = "";
            try
            {

                if (INDIVIDUO.Text.Length == longit)
                {
                    contafeno = 0;
                    for (int i = 0; i < longit; i = i + Convert.ToInt32(CPFENOTIPO.Text))
                    {
                        a = INDIVIDUO.Text.Substring(i, Convert.ToInt32(CPFENOTIPO.Text));
                        //MessageBox.Show(a);
                        individuos[0, conta, contafeno] = a;
                        contafeno++;
                    }
                    conta++;
                    REGISINDI.Text = "REGISTRAR A INDIVIDUO " + (conta + 1);
                
                    if (conta == Convert.ToInt32(POBLACION.Text))
                    {
                        REGISINDI.Enabled = false;
                        REGISINDI.Text = "POBLACION COMPLETA";
                        button2.Visible = true;
                        INDIVIDUO.Enabled = false;
                        button3.Enabled = false;
                    }
                }
                else
                {
                    MessageBox.Show("REGISTRO FALLIDO, NO CONTIENE LOS CARACTERES REQUERIDOS, DEBE TENER " + longit);
                    INDIVIDUO.Text = "";
                }
            }
            catch (Exception)
            {
                MessageBox.Show("A OCURRIDO UN ERROR");

            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            string a;
            int conta = 0;
            evaluador = new string[Convert.ToInt32(LONGCFENOTIPO.Text)];
            if (CONJUNTOFIT.Text.Length == longit)
            {
                for (int i = 0; i < longit; i = i + Convert.ToInt32(CPFENOTIPO.Text))
                {
                    a = CONJUNTOFIT.Text.Substring(i, Convert.ToInt32(CPFENOTIPO.Text));
                    //MessageBox.Show(a);
                    evaluador[conta] = a;
                    conta++;
                }
                sacafit();
                button2.Enabled = false;
                CONJUNTOFIT.Enabled = false;
            }
            else
            {
                MessageBox.Show("LONGITUD DE CONJUNTO EVALUADOR INVALIDA");
            }
        }

        private void INDIVIDUO_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
                e.Handled = false;
            else if (Char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        public void muestramatorder(int gen)
        {
            try
            {

                TEXTORICO.Text = TEXTORICO.Text + "\n\nSOBREVIVIENTES DE GENERACIÓN NUMERO  : " + (gen + 1) + "\n";
                for (int i2 = 0; i2 < sobrevivientes; i2++)
                {
                    TEXTORICO.Text = TEXTORICO.Text + "\nINDIVIDUO : " + (matorder[gen, i2, 0] + 1) + "        CON FITNESS DE :" + matorder[gen, i2, 1];

                }
            }
            catch (Exception)
            {
                MessageBox.Show("A OCURRIDO UN ERROR 3");
            }
        }

        public void muestramat()
        {
            try
            {
                for (int i1 = 0; i1 < Convert.ToInt32(GENERACIONES.Text); i1++)
                {
                    TEXTORICO.Text = TEXTORICO.Text + "\n" + i1 + "gene\n\n";
                    for (int i2 = 0; i2 < Convert.ToInt32(POBLACION.Text); i2++)
                    {
                        TEXTORICO.Text = TEXTORICO.Text + i2 + "indi\n";
                        for (int i3 = 0; i3 < Convert.ToInt32(LONGCFENOTIPO.Text); i3++)
                        {
                            TEXTORICO.Text = TEXTORICO.Text + individuos[i1, i2, i3] + "-";
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("A OCURRIDO UN ERROR 3");
            }
        }
}
}



