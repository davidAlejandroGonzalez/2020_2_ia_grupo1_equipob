﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioPOO
{
    class Alumno
    {
        string NumControl;
        string Nombre;
        string Carrera;
        string Escuela;

        int Semestre;
        int Promedio;
        int Creditos;

        bool Sexo;
        int Edad;

        List<Materia> Materias;

        public Alumno(string NumControl, string Nombre)
        {
            this.NumControl = NumControl;
            this.Nombre = Nombre;
            Semestre = 1;
            Promedio = 0;
            Creditos = 0;
            Materias = new List<Materia>();
        }

        public int calcularPromedio()
        {
            int acumuladorCalificaciones = 0;
            for (int i = 0; i < Materias[i].GetCalificacion(); i++)
            {
                Promedio = acumuladorCalificaciones / Materias.Count;
            }
            return Promedio;
        }

        public void agregarMateria(Materia MateriaNueva)
        {
            Materias.Add(MateriaNueva);
        }

        public string CursarMateria(string ClaveMateria, int Calificacion)
        {
            string error;
            for (int i = 0; i < Materias.Count; i++)
            {
                error = Materias[i].CursarMateria(ClaveMateria, Calificacion);
                if (error=="")
                return "";
                if (error != "No es la misma materia")
                    return error;                
            }
            return "El alumno no esta inscrito a la materia";
        }

        public int ContabilizarCreditos()
        {
            Creditos = 0;
            for (int i = 0; i < Materias.Count; i++)
            {
                Creditos += Materias[i].GetCreditos();
            }
            return Creditos;
        }

        public string GetDatosMaterias()
        {
            StringBuilder datos = new StringBuilder();
            for (int i = 0; i < Materias.Count; i++)
            {
                datos.Append(Materias[i].GetDatosMateria()+"\n");
            }
            return datos.ToString();
        }
    }
}
