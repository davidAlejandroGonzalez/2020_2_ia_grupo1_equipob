﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.XPath;

namespace K_Medias
{
    public partial class Form1 : Form
    {
        LectorDatos MiLectoDatos;
        List<Registro> centroidesSeleccionados = new List<Registro>();
        List<float> aux = new List<float>();
        List<Cluster> clusters = new List<Cluster>();



        public Form1()
        {
            InitializeComponent();


        }



        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OFDTexto.ShowDialog() == DialogResult.OK)
            {
                MiLectoDatos = new LectorDatos(OFDTexto.FileName);

                RTBTextoLeido.Text = MiLectoDatos.GetTextoLeido();
            }
            //en dado caso que se agregue un archivo nuevo, los comboBox se tienen que actualizar con los nuevos registros
            //se obliga al usuario a volver a checkear el checkBox para actualizar.

        }

        private void OFDTexto_FileOk(object sender, CancelEventArgs e)
        {

        }








        private void iterarBoton_Click(object sender, EventArgs e)
        {
            procesarCentroides();
        }





        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        public void procesarCentroides()
        {

            

            for (int i = 0; i < centroidesSeleccionados.Count; i++)
            {
                Cluster cluster = new Cluster("C" + i);
                clusters.Add(cluster);
            }
           
             bool b = true;
            string mostrar ="";

            do
            {
                aux.Clear();
                

                for (int i = 0; i < centroidesSeleccionados.Count; i++)
                {
                   for(int j =0; j<centroidesSeleccionados[i].Datos.Count;j++)
                    aux.Add(centroidesSeleccionados[i].Datos[j]);
                   
                }

               /* MessageBox.Show("AÑADIDO");*/

                for(int i=0; i < clusters.Count; i++)
                {
                    clusters[i].Registros.Clear();
                }

                
                for (int i = 0; i < MiLectoDatos.registrosHechos.Count; i++)
                {
                    float menorDistancia = 0;
                    int indiceMenor = 0;
                    float aux;
                    

                    for (int y = 0; y < centroidesSeleccionados.Count; y++)
                    {
                        aux = centroidesSeleccionados[y].CalcularDistancia(MiLectoDatos.registrosHechos[i]);
                        /*MessageBox.Show("Auxiliar: "+aux.ToString());*/
                        

                        if (aux < menorDistancia & y>0 )
                        {
                           /* MessageBox.Show("el menor es: "+aux);*/
                            
                            indiceMenor = y;
                        }
                        
                        
                        
                        
                        menorDistancia = aux;


                    }

                   /* MessageBox.Show("indice menor: "+indiceMenor.ToString());*/
                    clusters[indiceMenor].Registros.Add(MiLectoDatos.registrosHechos[i]);

                }

                for (int i = 0; i < clusters.Count; i++)
                {

                    for (int z = 0; z < MiLectoDatos.registrosHechos[0].Datos.Count; z++)
                    {
                        float promediador = 0f;



                        for (int y = 0; y < clusters[i].Registros.Count; y++)
                        {
                            promediador += clusters[i].Registros[y].Datos[z];
                        }

                        if(promediador != 0)
                        { 
                            centroidesSeleccionados[i].Datos[z] = ((promediador / clusters[i].Registros.Count));
                        }
                        else
                        {
                            centroidesSeleccionados[i].Datos[z] = (promediador);
                        }
                       

                        
                    }

                }

                


                b = true;

                int x = 0;

                for (int i = 0; i < centroidesSeleccionados.Count; i++)
                {

                    for (int y=0; y < centroidesSeleccionados[i].Datos.Count; y++)
                    {

                        /*MessageBox.Show("lEGAMOS A COMPARAR:"+clusters[i].Nombre+": "+centroidesSeleccionados[i].Datos[y] + " y " + aux[x] +"");*/
                        if (centroidesSeleccionados[i].Datos[y] != aux[x])
                        {
                            b = false;

                            

                        }
                        x++;
                    }

                }

                mostrar ="";
                for (int y = 0; y < clusters.Count; y++)
                {
                    mostrar += "Los registros que pertenecen a: " + clusters[y].Nombre + "\n";
                    for (int i = 0; i < clusters[y].Registros.Count; i++)
                    {
                        mostrar += clusters[y].Registros[i].GetID() + "\n";

                    }


                    mostrar += "________________________________________________________________________" + "\n";
                    mostrar += "\n";
                }

                richTextBox2.Text = mostrar;


            } while (b==false);


           



        }






        private void crearCentroide_Click(object sender, EventArgs e)
        {
            List<int> aleatorio = new List<int>();
            centroidesSeleccionados.Clear();
            clusters.Clear();
            Random r = new Random();
            int random = 0;

            if (aleatoriosCentroides.Checked == true)
            {



                for (int u = 0; u < centroidesCuantos.Value; u++)
                {
                    random = r.Next(0, MiLectoDatos.registrosHechos.Count);

                    if (!Repetidos(aleatorio, random))
                    {
                        centroidesSeleccionados.Add(MiLectoDatos.registrosHechos[random]);
                        aleatorio.Add(random);
                       
                    }
                    else
                    {
                        u--;
                    }

                }
               
               

                

            }
            else
            {
                for (int u = 0; u < centroidesCuantos.Value; u++)
                {
                    Registro centroide = new Registro();
                    centroidesSeleccionados.Add(centroide);
                    centroidesSeleccionados[u].SetID(Microsoft.VisualBasic.Interaction.InputBox("Ingrese el identificador del Centroide " + u + ":", "Ingrese dato"));

                    for (int y = 0; y < MiLectoDatos.registrosHechos[0].Datos.Count; y++)
                    {
                        centroidesSeleccionados[u].Datos.Add(float.Parse(Microsoft.VisualBasic.Interaction.InputBox("Ingrese el dato numero " + (y + 1) + " de " + centroidesSeleccionados[u].GetID() + ":", "► Creación de centroides ◄")));
                    }

                    MessageBox.Show("Datos de " + centroidesSeleccionados[u].GetID() + " finalizados");
                }

                MessageBox.Show("►LOS CENTROIDES HAN SIDO CREADOS EXITOSAMENTE◄");

            }
        }

        public bool Repetidos(List<int> repetidos, int numRepetido)
        {
            for (int i = 0; i < repetidos.Count; i++)
            {
                if (numRepetido == repetidos[i])
                {
                    return true;
                }

            }
            return false;
        }

        
    }

}
