﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace K_Medias
{
    class Registro
    {
        string Identificador;
        public List<float> Datos = new List<float>();
        //string listaactual;

        public Registro(string stringFuente)
        {
            //Como se tiene identificar cuales son los datos y cual es el identificador se crearon dos variables
            //una que almacene los char de los datos y otra del identificador
            string guardarNumeros="";
            string guardarIdentificador="";
            
            //Aqui se recorre la linea actual
            for (int i=0;i<stringFuente.Length;i++)
            {
                //Si es una letra, estamos hablando de que es el identificador, y por lo tanto se agrega a la variable guardarIdentificador
                if (Char.IsLetter(stringFuente[i])==true)
                {
                    guardarIdentificador += stringFuente[i];
                }
                //Si es un numero o un punto entonces se almacena en la variable guardar numeros
                else if ( Char.IsNumber(stringFuente[i]) == true | stringFuente[i]=='.')
                {
                    guardarNumeros += stringFuente[i];
                }
                //como estan separados por comas entonces significa que se completó de leer un dato
                else if (stringFuente[i] == ',' )
                {
                    //Aqui se verifica que la variable guardar numeros no sea nula, si esto es correcto quiere decir que ha almacenado un dato
                    //entonces se agrega a la lista Datos y guardarNumeros vuelve a ser vacia para almacenar nuevos datos o si en caso contrario
                    //ya no los hay o encontró letras, pues estas no se agreguen.
                    if (guardarNumeros != "") {
                        Datos.Add(float.Parse(guardarNumeros)); 
                        guardarNumeros = "";
                        
                    }
                    //La misma lógica usada, si la variable no es nula, eso quiere decir que encontró letras, entonces la variable Identificador
                    //recibe el valor que tiene guardadIdentificador y la variable guardarIdentificador vuelve a estar vacia
                    if (guardarIdentificador != "")
                    {
                        Identificador = guardarIdentificador;
                        guardarIdentificador = "";
                    }
                }
                //En el caso de que ya no haya una coma y se haya leido el ultimo dato, entonces podemos decir que i+1 es igual a
                //el numero de caracteres que contiene stringFuente, eso quiere decir que estamos en la parte final y por lo tanto se agregan
                //los datos.
                if (i + 1 == stringFuente.Length)
                {
                    if (guardarNumeros != "")
                    {
                        Datos.Add(float.Parse(guardarNumeros));
                        guardarNumeros = "";

                    }

                }



            }





          

        }

        public Registro()
        {

        }

        public float CalcularDistancia(Registro OtroRegistro)
        {
            float distancia = 0f;
            //Esta variable se utilizará para hacer la suma de los cuadrados y despues sacar la raíz.
            float sumaCuadrados = 0f;
            //Creamos una lista para guardar las restas 
            List<float> restaResultado = new List<float>();
            //Asignamos las restas a la lista restaResultado
            for (int i=0;i<OtroRegistro.Datos.Count;i++ )
            {
                restaResultado.Add(Datos[i] - OtroRegistro.Datos[i]);
            }

            for(int i = 0; i < restaResultado.Count; i++)
            {
                //el casting se hizo para almacenar el resultado float en la variable sumaCuadrados ya que Math.Pow regresa el resultado en double
                sumaCuadrados += (float) Math.Pow(restaResultado[i], 2);
            }
            
            return distancia = (float)Math.Sqrt(sumaCuadrados);
        }

        
        
        public string MostrarDatosPrueba()
        {
            string datos="";

            for(int i = 0; i < Datos.Count; i++)
            {
                datos += Convert.ToString(Datos[i]);
                datos += " ";
            }

            return datos;
        }
       
        public string GetID()
        {
            return Identificador;
        }

        public void SetID(String id)
        {
            this.Identificador = id;
        }


    }
}
