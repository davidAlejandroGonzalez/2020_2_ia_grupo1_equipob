﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjercicioPOO
{
    public partial class Form1 : Form
    {
        Alumno ElAlumno = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnInscribir_Click(object sender, EventArgs e)
        {
            ElAlumno = new Alumno(txtClave.Text, txtNombre.Text);
            btnAgregarMat.Enabled = true;
        }

        private void btnAgregarMat_Click(object sender, EventArgs e)
        {
            FDatosMateria ventanaMateria = new FDatosMateria();
            if(ventanaMateria.ShowDialog() ==DialogResult.OK)
            ElAlumno.agregarMateria(ventanaMateria.GetMateria());
            RTBMaterias.Text = ElAlumno.GetDatosMaterias();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnCursarMateria_Click(object sender, EventArgs e)
        {
            if (ElAlumno.CursarMateria(txtClave.Text, (int)NUDCalificacion.Value))
            {
                MessageBox.Show("No fue posible cursar la materia");
            }
            else
            {
                MessageBox.Show("Materia cursada correctamente");
            }
        }
    }
}
