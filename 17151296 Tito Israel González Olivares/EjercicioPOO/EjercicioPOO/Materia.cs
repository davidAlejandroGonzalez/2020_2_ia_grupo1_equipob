﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioPOO
{
    public class Materia
    {
        string Clave;
        string Nombre;
        string Profesor;
        string Departamento;

        int Intentos;
        int Creditos;
        int Calificacion;

        bool EsDeEspecialidad;

        public Materia(string Clave, string Nombre, int Creditos)
        {
            this.Clave = Clave;
            this.Nombre = Nombre;
            this.Creditos = Creditos;
            Calificacion = 0;
            Intentos = 1;
        }

        public string CursarMateria(string ClaveMateriaCurso, int Calificacion)
        {
            if (ClaveMateriaCurso != Clave)
            {
                return "No es la misma materia";
            }

            if (YaEstaAprobada())
            {
                return "Ya se aprobo previamente la materia";
            }

            if (IntentosAgotados())
            {
               return "Oportunidades de cursamiento agotadas";
            }
               
            if (Calificacion < 70)
            {
               Calificacion = 0;
               Intentos++;
            }
            else
            {
               this.Calificacion = Calificacion;
            }
            return "";                         
        }

        public bool YaEstaAprobada()
        {
            return Calificacion > 69;
        }

        public bool IntentosAgotados()
        {
            return Intentos>3;
        }

        public bool YaCursoLaMateria()
        {
            return Intentos > 1 || YaEstaAprobada();
        }

        public int GetCalificacion()
        {
            return Calificacion;
        }

        public int GetCreditos()
        {
            return YaEstaAprobada() ? Creditos : 0;
        }

        public string GetDatosMateria()
        {
            string datos = "";
            datos += Nombre + " , clave: " + Clave + ", Créditos: " + Creditos;
            return datos;
        }
    }
}
