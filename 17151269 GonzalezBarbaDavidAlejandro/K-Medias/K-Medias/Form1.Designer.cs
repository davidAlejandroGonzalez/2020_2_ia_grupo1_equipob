﻿namespace K_Medias
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OFDTexto = new System.Windows.Forms.OpenFileDialog();
            this.RTBTextoLeido = new System.Windows.Forms.RichTextBox();
            this.De = new System.Windows.Forms.Label();
            this.iterarBoton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.centroidesCuantos = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.crearCentroide = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.aleatoriosCentroides = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.centroidesCuantos)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(849, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem});
            this.archivoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.archivoToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(90, 29);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(127, 30);
            this.abrirToolStripMenuItem.Text = "Abrir";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // OFDTexto
            // 
            this.OFDTexto.FileName = "openFileDialog1";
            this.OFDTexto.Filter = "Archivos de texto| *.txt|Valores separados por comas|*.csv|Todos los archivos|*.*" +
    "";
            this.OFDTexto.FileOk += new System.ComponentModel.CancelEventHandler(this.OFDTexto_FileOk);
            // 
            // RTBTextoLeido
            // 
            this.RTBTextoLeido.BackColor = System.Drawing.Color.White;
            this.RTBTextoLeido.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RTBTextoLeido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBTextoLeido.ForeColor = System.Drawing.Color.Teal;
            this.RTBTextoLeido.Location = new System.Drawing.Point(0, 60);
            this.RTBTextoLeido.Margin = new System.Windows.Forms.Padding(2);
            this.RTBTextoLeido.Name = "RTBTextoLeido";
            this.RTBTextoLeido.Size = new System.Drawing.Size(266, 283);
            this.RTBTextoLeido.TabIndex = 1;
            this.RTBTextoLeido.Text = "";
            // 
            // De
            // 
            this.De.AutoSize = true;
            this.De.BackColor = System.Drawing.Color.DarkCyan;
            this.De.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.De.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.De.Location = new System.Drawing.Point(72, 1);
            this.De.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.De.Name = "De";
            this.De.Size = new System.Drawing.Size(154, 25);
            this.De.TabIndex = 11;
            this.De.Text = "Elegir centroides";
            // 
            // iterarBoton
            // 
            this.iterarBoton.BackColor = System.Drawing.Color.Aquamarine;
            this.iterarBoton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.iterarBoton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iterarBoton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iterarBoton.Location = new System.Drawing.Point(77, 270);
            this.iterarBoton.Margin = new System.Windows.Forms.Padding(2);
            this.iterarBoton.Name = "iterarBoton";
            this.iterarBoton.Size = new System.Drawing.Size(126, 29);
            this.iterarBoton.TabIndex = 16;
            this.iterarBoton.Text = "Procesar";
            this.iterarBoton.UseVisualStyleBackColor = false;
            this.iterarBoton.Click += new System.EventHandler(this.iterarBoton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Teal;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(-4, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(270, 25);
            this.label1.TabIndex = 17;
            this.label1.Text = "                 Registros                   ";
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.ForeColor = System.Drawing.Color.Teal;
            this.richTextBox2.Location = new System.Drawing.Point(561, 60);
            this.richTextBox2.Margin = new System.Windows.Forms.Padding(2);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(288, 283);
            this.richTextBox2.TabIndex = 14;
            this.richTextBox2.Text = "";
            this.richTextBox2.TextChanged += new System.EventHandler(this.richTextBox2_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Teal;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(559, 33);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(290, 25);
            this.label3.TabIndex = 18;
            this.label3.Text = "                    Clusters                      ";
            // 
            // centroidesCuantos
            // 
            this.centroidesCuantos.Location = new System.Drawing.Point(142, 54);
            this.centroidesCuantos.Name = "centroidesCuantos";
            this.centroidesCuantos.Size = new System.Drawing.Size(61, 20);
            this.centroidesCuantos.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.DarkCyan;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(73, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 21);
            this.label2.TabIndex = 20;
            this.label2.Text = "Cuantos: ";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkCyan;
            this.panel1.Controls.Add(this.crearCentroide);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.aleatoriosCentroides);
            this.panel1.Controls.Add(this.centroidesCuantos);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.iterarBoton);
            this.panel1.Controls.Add(this.De);
            this.panel1.Location = new System.Drawing.Point(266, 32);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(299, 310);
            this.panel1.TabIndex = 21;
            // 
            // crearCentroide
            // 
            this.crearCentroide.BackColor = System.Drawing.Color.PaleTurquoise;
            this.crearCentroide.Cursor = System.Windows.Forms.Cursors.Hand;
            this.crearCentroide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.crearCentroide.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crearCentroide.Location = new System.Drawing.Point(77, 130);
            this.crearCentroide.Margin = new System.Windows.Forms.Padding(2);
            this.crearCentroide.Name = "crearCentroide";
            this.crearCentroide.Size = new System.Drawing.Size(126, 30);
            this.crearCentroide.TabIndex = 23;
            this.crearCentroide.Text = "Crear centroides";
            this.crearCentroide.UseVisualStyleBackColor = false;
            this.crearCentroide.Click += new System.EventHandler(this.crearCentroide_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.DarkCyan;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(73, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 21);
            this.label4.TabIndex = 22;
            this.label4.Text = "Aleatorios:";
            // 
            // aleatoriosCentroides
            // 
            this.aleatoriosCentroides.AutoSize = true;
            this.aleatoriosCentroides.Location = new System.Drawing.Point(162, 95);
            this.aleatoriosCentroides.Name = "aleatoriosCentroides";
            this.aleatoriosCentroides.Size = new System.Drawing.Size(15, 14);
            this.aleatoriosCentroides.TabIndex = 21;
            this.aleatoriosCentroides.UseVisualStyleBackColor = true;
            this.aleatoriosCentroides.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(849, 342);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.RTBTextoLeido);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "K-Medias";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.centroidesCuantos)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog OFDTexto;
        private System.Windows.Forms.RichTextBox RTBTextoLeido;
        private System.Windows.Forms.Label De;
        private System.Windows.Forms.Button iterarBoton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown centroidesCuantos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button crearCentroide;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox aleatoriosCentroides;
    }
}

