﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjercicioPOO
{
    public partial class FDatosMateria : Form
    {
        Materia LaMateria = null;
        public FDatosMateria()
        {
            InitializeComponent();
        }

        private void FDatosMateria_Load(object sender, EventArgs e)
        {

        }

        private void btnCrearMateria_Click(object sender, EventArgs e)
        {
            LaMateria = new Materia(txtClaveMateria.Text, txtNombreMateria.Text, (int)nUDCreditos.Value);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public Materia GetMateria()
        {
            return LaMateria;
        }
    }
}
